// Import necessary modules
const fastify = require('fastify')();
import  { deposit, withdraw } from  '../banking';

// Define controller functions
async function depositMethod(request, reply) {
    // Extract amount and user information from request
    const { user, amount } = request.body;
    try {
        const newBalance = deposit(user, amount);
        return { balance: newBalance };
    } catch (error) {
        reply.status(400).send({ error: error.message });
    }
}

async function withdrawMethod(request, reply) {
    // Extract amount and user information from request
    const { user, amount } = request.body;
    try {
        const newBalance = withdraw(user, amount);
        return { balance: newBalance };
    } catch (error) {
        reply.status(400).send({ error: error.message });
    }
}

async function transferMethod(request, reply) {
    const { senderId, recipientId, amount } = request.body;
    try {
        const sender = senderId === user1.id ? user1 : user2;
        const recipient = recipientId === user1.id ? user1 : user2;
        const result = transfer(sender, recipient, amount);
        reply.send({ message: 'Transfer successful', result });
    } catch (error) {
        reply.code(400).send({ error: error.message });
    }
}

// Define routes
fastify.post('/deposit', depositMethod);
fastify.post('/withdraw', withdrawMethod);

fastify.post('/transfer', transferMethod);

module.exports = fastify;
